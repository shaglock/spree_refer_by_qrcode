module SpreeReferByQrcode
  module Spree
    module UserRegistrationsControllerDecorator
      def self.prepended(base)
        base.layout 'spree/layouts/new_login'
        base.before_action :check_referrer_and_set_session, only: :new
      end

      def create
        @user = build_resource(spree_user_params)
        resource_saved = resource.save
        yield resource if block_given?
        if resource_saved
          session.delete(:referred_by_id)

          if resource.active_for_authentication?
            set_flash_message :notice, :signed_up
            sign_up(resource_name, resource)
            session[:spree_user_signup] = true
            redirect_to_checkout_or_account_path(resource)
          else
            set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}"
            expire_data_after_sign_in!
            respond_with resource, location: after_inactive_sign_up_path_for(resource)
          end
        else
          clean_up_passwords(resource)
          render :new
        end
      end

      def check_referrer_and_set_session
        if session[:referred_by_id].blank? && params[:referred_by_id].blank?
          raise ActionController::RoutingError.new('Not Found')
        end

        return if session[:referred_by_id] == params[:referred_by_id]

        session[:referred_by_id] = params[:referred_by_id]
      end

      private

      def spree_user_params
        byebug
        super.merge(referred_by_id: session[:referred_by_id])
      end
    end
  end
end

::Spree::UserRegistrationsController.prepend SpreeReferByQrcode::Spree::UserRegistrationsControllerDecorator if ::Spree::UserRegistrationsController.included_modules.exclude?(SpreeReferByQrcode::Spree::UserRegistrationsControllerDecorator)