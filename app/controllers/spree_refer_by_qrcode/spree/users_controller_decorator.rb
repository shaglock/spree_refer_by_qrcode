module SpreeReferByQrcode
  module Spree
    module UsersControllerDecorator
      def qr_code
        qr = RQRCode::QRCode.new(
          spree.signup_url(
            host: ENV.fetch("DEFAULT_HOST") { 'localhost:3000' },
            referred_by_id: params[:id]
          )
        )
        render inline: qr.as_svg
      end
    end
  end
end

::Spree::UsersController.prepend SpreeReferByQrcode::Spree::UsersControllerDecorator if ::Spree::UsersController.included_modules.exclude?(SpreeReferByQrcode::Spree::UsersControllerDecorator)